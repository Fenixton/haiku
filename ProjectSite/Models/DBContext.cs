﻿using System.Data.Entity;

namespace ProjectSite.Models
{
    public class DBContext : DbContext
    {
        public DBContext()
        {
            Database.SetInitializer<DBContext>(null);
        }

        public DbSet<Haiku> Haikus { get; set; }
    }
}