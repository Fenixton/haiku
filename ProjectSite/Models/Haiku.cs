﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace ProjectSite.Models
{
    [Table("Haiku")]
    public class Haiku
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Пожалуйста, введите текст хайку")]
        [Display(Name = "Хайку")]
        public string Content { get; set; }

        [Display(Name = "Дата")]
        public DateTime Date { get; set; }

        [HiddenInput]
        public string UserId { get; set; }

        [HiddenInput]
        public string UserName { get; set; }
    }
}