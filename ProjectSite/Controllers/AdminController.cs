﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Net;
using System.Web.Mvc;
using ProjectSite.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace ProjectSite.Controllers
{
    [Authorize(Roles = "Администратор")]
    public class AdminController : Controller
    {
        private ApplicationDbContext db0 = new ApplicationDbContext();
        // GET: Admin
        public ActionResult Index()
        {
            ViewBag.IsAdmin = IsAdmin();
            var users = db0.Users.ToList();
            return View(users);
        }

        [HttpGet]
        public ActionResult Delete(string id)
        {
            ViewBag.IsAdmin = IsAdmin();

            if (String.IsNullOrEmpty(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser user = db0.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            return View(user);
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            var manager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = manager.FindById(id);
            if (user != null)
            {
                IdentityResult result = manager.Delete(user);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Admin");
                }
            }
            return RedirectToAction("Index", "Admin");
        }

        private bool IsAdmin()
        {
            var IsAdmin = false;
            if (User.Identity.IsAuthenticated)
            {
                var manager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var user = manager.FindByEmail(User.Identity.Name);
                IsAdmin = manager.GetRoles(user.Id).Contains("Администратор");
            }
            return IsAdmin;
        }
    }
}