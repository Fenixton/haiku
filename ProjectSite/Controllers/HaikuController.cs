﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Data.Entity;
using System.Web.Mvc;
using ProjectSite.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace ProjectSite.Controllers
{
    public class HaikuController : Controller
    {
        private DBContext db = new DBContext();
        private ApplicationDbContext db0 = new ApplicationDbContext();

        // GET: Haiku
        public ActionResult Index()
        {
            ViewBag.IsAdmin = false;
            if (User.Identity.IsAuthenticated)
            {
                var user = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByEmail(User.Identity.Name);
                ViewBag.MyId = user.Id;
                ViewBag.MyName = user.Name;
                ViewBag.IsAdmin = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().GetRoles(user.Id).Contains("Администратор");
            }
            return View(db.Haikus.ToList());
        }

        // GET: Haiku/Create
        [Authorize]
        public ActionResult Create()
        {
            ViewBag.IsAdmin = IsAdmin();
            return View();
        }

        // POST: Haiku/Create
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Content,Date,UserId,UserName")] Haiku haiku)
        {
            if (ModelState.IsValid)
            {
                var user = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByEmail(User.Identity.Name);

                haiku.UserId = user.Id;
                haiku.UserName = user.Name;
                haiku.Date = DateTime.Now;
                db.Haikus.Add(haiku);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IsAdmin = IsAdmin();
            return View(haiku);
        }

        // GET: Haiku/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Haiku haiku = db.Haikus.Find(id);
            if (haiku == null)
            {
                return HttpNotFound();
            }
            if (haiku.UserId != HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByEmail(User.Identity.Name).Id && !IsAdmin())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            ViewBag.IsAdmin = IsAdmin();
            return View(haiku);
        }

        // POST: Haiku/Edit/5
        [Authorize]
        [HttpPost]
        public ActionResult Edit([Bind(Include = "Id,Content,Date,UserId,UserName")] Haiku haiku)
        {
            if (ModelState.IsValid)
            {
                var oldHaiku = db.Haikus.Find(haiku.Id);
                oldHaiku.Content = haiku.Content;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IsAdmin = IsAdmin();
            return View(haiku);
        }

        // GET: Haiku/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Haiku haiku = db.Haikus.Find(id);
            if (haiku == null)
            {
                return HttpNotFound();
            }
            if (haiku.UserId != HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByEmail(User.Identity.Name).Id && !IsAdmin())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            ViewBag.IsAdmin = IsAdmin();
            return View(haiku);
        }

        // POST: Haiku/Delete/5
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            ViewBag.IsAdmin = IsAdmin();
            Haiku haiku = db.Haikus.Find(id);
            db.Haikus.Remove(haiku);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        private bool IsAdmin()
        {
            var IsAdmin = false;
            if (User.Identity.IsAuthenticated)
            {
                var manager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var user = manager.FindByEmail(User.Identity.Name);
                IsAdmin = manager.GetRoles(user.Id).Contains("Администратор");
            }
            return IsAdmin;
        }
    }
}
