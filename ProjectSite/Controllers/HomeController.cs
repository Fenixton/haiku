﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectSite.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace ProjectSite.Controllers
{
    public class HomeController : Controller
    {
        private DBContext db = new DBContext();

        public ActionResult Index()
        {
            ViewBag.IsAdmin = false;
            if (User.Identity.IsAuthenticated)
            {
                var user = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByEmail(User.Identity.Name);
                ViewBag.MyId = user.Id;
                ViewBag.MyName = user.Name;
                ViewBag.IsAdmin = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().GetRoles(user.Id).Contains("Администратор");
            }
            var haikus = db.Haikus.ToList();
            var rndHaikus = new List<Haiku>();
            if (haikus.Count > 8)
            {
                var rnd = new Random();
                while (rndHaikus.Count < 8)
                {
                    var nextHaiku = haikus[rnd.Next(haikus.Count)];
                    if (!rndHaikus.Contains(nextHaiku))
                        rndHaikus.Add(nextHaiku);
                }
            }
            else
            {
                rndHaikus = haikus;
            }
            return View(rndHaikus);
        }

        public ActionResult About()
        {
            ViewBag.IsAdmin = false;
            if (User.Identity.IsAuthenticated)
            {
                var manager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var user = manager.FindByEmail(User.Identity.Name);
                ViewBag.IsAdmin = manager.GetRoles(user.Id).Contains("Администратор");
            }
            return View();
        }

        public ActionResult HowTo()
        {
            ViewBag.IsAdmin = false;
            if (User.Identity.IsAuthenticated)
            {
                var manager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var user = manager.FindByEmail(User.Identity.Name);
                ViewBag.IsAdmin = manager.GetRoles(user.Id).Contains("Администратор");
            }
            return View();
        }
    }
}