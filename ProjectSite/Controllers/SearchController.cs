﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectSite.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace ProjectSite.Controllers
{
    public class SearchController : Controller
    {
        private DBContext db = new DBContext();
        
        // GET: /Search?SearchRequest=...
        public ActionResult Index(string SearchRequest)
        {
            if (String.IsNullOrEmpty(SearchRequest) || String.IsNullOrWhiteSpace(SearchRequest))
            {
                return Redirect("/");
            }
            var haikus = db.Haikus.ToList();
            var foundHaikus = new List<Haiku>();

            foreach (var haiku in haikus)
            {
                if (haiku.Content.ToLower().Contains(SearchRequest.ToLower()) || haiku.UserName.ToLower().Contains(SearchRequest.ToLower()))
                    foundHaikus.Add(haiku);
            }
            ViewBag.SearchRequest = SearchRequest;
            ViewBag.Haikus = foundHaikus;
            ViewBag.IsAdmin = false;
            if (User.Identity.IsAuthenticated)
            {
                var manager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var user = manager.FindByEmail(User.Identity.Name);
                ViewBag.MyId = user.Id;
                ViewBag.MyName = user.Name;
                ViewBag.IsAdmin = manager.GetRoles(user.Id).Contains("Администратор");
            }
            return View();
        }
    }
}